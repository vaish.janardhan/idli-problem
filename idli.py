#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 13 16:28:28 2019

@author: vaishnavi
"""
def find_odd_count(idli_list):
    num_of_odds = 0
    for num in idli_list:
        if num % 2:
            num_of_odds += 1
    return num_of_odds

def iter_idli_list(idli_list,num):
    for i in range(len(idli_list) - 1):
            if idli_list[i] % 2:
                idli_list[i] += 1
                idli_list[i + 1] += 1
                num += 2
                return iter_idli_list(idli_list,num)
    return num   
                
def find_idli_count(idli_list):
    n = 0
    odd_count = find_odd_count(idli_list)
    if odd_count == 0:
        return n
    elif odd_count % 2:
        return -1
    else:
        return iter_idli_list(idli_list,n)
                
idlis = [2,3,5,6,7]
print(find_idli_count(idlis))